package com.latam.pax.cybersrc.ssc.utils;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.lan.webservice.jaxws.cybersource.validateFraud.AFSService;
import com.lan.webservice.jaxws.cybersource.validateFraud.Item;
import com.lan.webservice.jaxws.cybersource.validateFraud.MDDField;
import com.lan.webservice.jaxws.cybersource.validateFraud.MerchantDefinedData;
import com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory;
import com.lan.webservice.jaxws.cybersource.validateFraud.RequestMessage;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.Leg;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.type.DateType;
import com.latam.pax.cybersrc.utils.DateUtils;
import com.latam.pax.cybersrc.utils.ParserCommonUtils;
import com.latam.pax.cybersrc.utils.PrintUtils;

public final class ParserRequestSSCUtils {

    public static final String FORMATDATE_Y_M_D_H_M = "yyyy-MM-dd HH:mm";
    public static final String FORMATDATE_Y_M_D = "yyyy-MM-dd";
        
    @Autowired
    private ParserRequestSSCUtils() {

    }
    
    public static void fillPrincipalData(ValidateFraudRQ request, RequestMessage requestCybersource, AFSService serviceAfs) {
        requestCybersource.setAfsService(serviceAfs);

        serviceAfs.setRun("true");
        requestCybersource.setMerchantID(request.getMerchantId());

    }

    public static void fillItems(ValidateFraudRQ request, ObjectFactory factoryCybersource,
            RequestMessage requestCybersource) {
        if (requestCybersource.getItem() != null) {
            List<Item> itemsCybersource = requestCybersource.getItem();
            List<com.latam.pax.cybersrc.domain.schemas.Item> itemsBody = request.getSaleInformation()
                    .getShoppingCartInformation().getItems().getItem();
            BigInteger itemId = BigInteger.ZERO;

            for (com.latam.pax.cybersrc.domain.schemas.Item itemBody : itemsBody) {
                Item itemCyberSource = factoryCybersource.createItem();
                itemCyberSource.setId(itemId);
                fillItemPassenger(itemBody, itemCyberSource);
                itemsCybersource.add(itemCyberSource);
                itemId = itemId.add(BigInteger.valueOf(1));
            }
        }
    }

    private static void fillItemPassenger(com.latam.pax.cybersrc.domain.schemas.Item itemBody, Item itemCyberSource) {

        itemCyberSource.setPassengerFirstName(itemBody.getPassengerInformation().getFirstName());
        itemCyberSource.setPassengerLastName(itemBody.getPassengerInformation().getLastName());
        itemCyberSource.setPassengerEmail(itemBody.getPassengerInformation().getEmail());
        itemCyberSource.setPassengerPhone(itemBody.getPassengerInformation().getPhoneNumber());
        itemCyberSource.setUnitPrice("0");
        if (!itemBody.getPassengerInformation().getDocuments().getDocument().isEmpty()) {
            itemCyberSource
                    .setPassengerID(itemBody.getPassengerInformation().getDocuments().getDocument().get(0).getId());
        }
    }

    public static void fillMerchantDefinedData(ValidateFraudRQ request, String issueType, String isPayed,
            String isDisruption, ObjectFactory factoryCybersource, RequestMessage requestCybersource) {
        DateUtils dateUtils = new DateUtils();
        MerchantDefinedData merchantDefinedData = factoryCybersource.createMerchantDefinedData();
        List<MDDField> mDDFields = merchantDefinedData.getMddField();
        ParserCommonUtils.setFieldSSC(GlobalConstant.ONE,
                request.getSaleInformation().getShoppingCartInformation().getItems().getItem().get(0).getPnrCode(),
                factoryCybersource, mDDFields);
        ParserCommonUtils.setFieldSSC(GlobalConstant.TWO, request.getSaleInformation().getSalesOfficeInformation().getCountryISOCode(),
                factoryCybersource, mDDFields);
        ParserCommonUtils.setFieldSSC(GlobalConstant.FIVE,
                String.valueOf(request.getSaleInformation().getSalesOfficeInformation().getSalesAgentCode()),
                factoryCybersource, mDDFields);
        ParserCommonUtils.setFieldSSC(GlobalConstant.SIX,
                String.valueOf(request.getSaleInformation().getSalesOfficeInformation().getStationNumber()),
                factoryCybersource, mDDFields);
        ParserCommonUtils.setFieldSSC(GlobalConstant.SEVEN, request.getLegs().getLeg().get(0).getFareClass(), factoryCybersource, mDDFields);
        PrintUtils.printInfoData("MDD 7");
        ParserCommonUtils.setFieldSSC(GlobalConstant.TWENTY_SEVEN, issueType, factoryCybersource, mDDFields);
        if (!request.getLegs().getLeg().isEmpty()) {
            ParserCommonUtils.setFieldSSC(GlobalConstant.THIRTY_ONE, request.getLegs().getLeg().get(0).getAirlineIATACode(),
                    factoryCybersource, mDDFields);
            ParserCommonUtils.setFieldSSC(GlobalConstant.THIRTY_THREE, request.getLegs().getLeg().get(0).getFlightNumber(),
                    factoryCybersource, mDDFields);
        }
        // G.Carcamo - Nuevos MDD - del 42 al 48
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_TWO, isPayed, factoryCybersource, mDDFields);
        // MDD 43 - Delta en horas de compra. (Reemision-Original)
        String reemisionShopDate = request.getSaleInformation().getCurrentDateTime();
        String originalShopDate = request.getSaleInformation().getExchangeTicketInformation().getIssueDate();
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_THREE,
                String.valueOf(
                        dateUtils.getDiference(dateUtils.transformDateTime(originalShopDate, FORMATDATE_Y_M_D_H_M),
                                dateUtils.transformDateTime(reemisionShopDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
                factoryCybersource, mDDFields);
        // MDD 44 - Delta en horas de fecha de vuelo (Reemison-Original)
        String originalFlightDate = request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()
                .get(0).getDepartureInformation().getDateTime();
        String reemisionFlightDate = request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime();
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_FOUR,
                String.valueOf(
                        dateUtils.getDiference(dateUtils.transformDateTime(reemisionFlightDate, FORMATDATE_Y_M_D_H_M),
                                dateUtils.transformDateTime(originalFlightDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
                factoryCybersource, mDDFields);
        // MDD45 -- Numero del primer cupon que se está cambiando
        int couponNumber = ParserCommonUtils.verifyDiffCoupons(request);
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_FIVE, String.valueOf(couponNumber), factoryCybersource, mDDFields);
        // MDD 46 - Numero del boleto original
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_SIX,
                request.getSaleInformation().getExchangeTicketInformation().getTicketNumber(), factoryCybersource,
                mDDFields);
        // MDD 47 - Ruta original - concatenacion de arival - departure de legs
        StringBuilder originalRoute = new StringBuilder();
        for (Leg cursorLeg : request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()) {
            if (!originalRoute.toString().isEmpty()) {
                originalRoute.append(GlobalConstant.STR_TWO_POINT);
            }
            originalRoute.append(cursorLeg.getDepartureInformation().getAirportIATACode() + GlobalConstant.STR_GUION
                    + cursorLeg.getArrivalInformation().getAirportIATACode());
        }
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_SEVEN, originalRoute.toString(), factoryCybersource, mDDFields);
        // MDD 48
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_EIGHT, String.valueOf(dateUtils.getDiference(dateUtils.transformDateTime(
                request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime(), FORMATDATE_Y_M_D_H_M),
                dateUtils.getDateServer(), DateType.HOUR)), factoryCybersource, mDDFields);
        // MDD 49
        ParserCommonUtils.setFieldSSC(GlobalConstant.FORTY_NINE, isDisruption, factoryCybersource, mDDFields);
        if (mDDFields != null && !mDDFields.isEmpty()) {
            requestCybersource.setMerchantDefinedData(merchantDefinedData);
        }

    } 
    

}
