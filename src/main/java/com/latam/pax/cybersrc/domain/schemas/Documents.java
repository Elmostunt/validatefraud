package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 */
@ToString
@EqualsAndHashCode

public class Documents implements Serializable {

    private static final long serialVersionUID = 4214323567535448972L;

    @Getter
    @Setter
    private List<Document> document;

    public Documents() {
        document = new ArrayList<>();
    }

}
