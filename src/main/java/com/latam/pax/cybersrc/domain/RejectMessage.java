package com.latam.pax.cybersrc.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <p>
 * Register of versions:
 * <ul>
 * <li>1.0 05-07-2016, (Everis Chile) - initial release
 * </ul>
 * <p>
 * Object domain, for the reject message
 * 
 * <p>
 * <B>All rights reserved by Lan.</B>
 */
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RejectMessage implements Serializable {

    private static final long serialVersionUID = 983333186226777761L;

    @NotNull(message = "{not.null.attribute}")
    @Getter
    @Setter
    private long reasonCode;
    @Getter
    @Setter
    private String invalidField;
    @Getter
    @Setter
    private String descriptionError;
}
