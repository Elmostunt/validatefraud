package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <p>
 * Register of versions:
 * <ul>
 * <li>1.0 05-07-2016, (Everis Chile) - initial release
 * </ul>
 * <p>
 * This class contains the domain object
 * 
 * <p>
 * <B>All rights reserved by Lan.</B>
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Leg implements Serializable {

    private static final long serialVersionUID = -3904115178237599549L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private String couponNumber;
    @Getter
    @Setter
    private String airlineIATACode;
    @Getter
    @Setter
    private String flightNumber;
    @Getter
    @Setter
    private String fareClass;
    @Getter
    @Setter
    private DepartureInformation departureInformation;
    @Getter
    @Setter
    private ArrivalInformation arrivalInformation;

}
