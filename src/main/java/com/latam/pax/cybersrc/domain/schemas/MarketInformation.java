package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 */
@ToString
@EqualsAndHashCode
public class MarketInformation implements Serializable {

	private static final long serialVersionUID = -5660073386458816214L;

	@NotNull(message = "{domain.not.null}")
	@Getter
	@Setter
	private String merchantId;

}
