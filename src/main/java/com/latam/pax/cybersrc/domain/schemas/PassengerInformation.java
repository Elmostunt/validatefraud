package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class PassengerInformation implements Serializable {

    private static final long serialVersionUID = 4173726791365382672L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private Documents documents;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String phoneNumber;

}
