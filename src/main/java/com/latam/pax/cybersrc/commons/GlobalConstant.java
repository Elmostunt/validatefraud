package com.latam.pax.cybersrc.commons;

public final class GlobalConstant {
	// G.Carcamo ------------- Constantes Cybersource BR
	// -----------------------------------------------------------------
public static final String WS_MERCHANT_ID_PROPERTIES = "merchantid.br";
public static final String WS_URL_TAM_CYBERSOURCE_PROPERTIES = "ws.tam.url.cybersource";
public static final String WS_SECURITY_TAM_BR_USERNAME = "ws.tam.security.username.br";
public static final String WS_SECURITY_TAM_INT_USERNAME = "ws.tam.security.username.int";
public static final String WS_SECURITY_TAM_BR_CODE_BASE64_TEXT = "ws.tam.security.password.base64text.br";
public static final String WS_SECURITY_TAM_INT_CODE_BASE64_TEXT = "ws.tam.security.password.base64text.int";

	// Constantes para QNAME
public static final String WS_QNAME_ATTRIBUTE_ONE = "urn:schemas-cybersource-com:transaction-data:TransactionProcessor";
public static final String WS_QNAME_ATTRIBUTE_TWO = "TransactionProcessor";
	// Endpoint used in cybersorce client class
public static final String WS_ENDPOINT_TAM_CYBERSOURCE_PROPERTIES = "ws.tam.endpoint.cybersource";

	// ----------------------------------------------------------------------------------------------------------------------

	// G.Carcamo ------------- Constantes Cybersource SSC
	// -----------------------------------------------------------------
	// INFO SERVICE LAN CYBERSOURCE
public static final String WS_URL_LAN_CYBERSOURCE_PROPERTIES = "ws.lan.url.cybersource";
public static final String WS_SECURITY_LAN_USERNAME = "ws.lan.security.username";
public static final String WS_SECURITY_LAN_CODE_BASE64_TEXT = "ws.lan.security.password.base64text";
public static final String WS_ENDPOINT_LAN_CYBERSOURCE_PROPERTIES = "ws.lan.endpoint.cybersource";
public static final String EMISSION = "E";
public static final String REMISSION = "R";
public static final String STR_TRUE = "true";
public static final String UNITPRICE = "0";
public static final String STR_TWO_POINT = ":";
public static final String STR_GUION = "-";
public static final String WS_CYBSERSOURCE_LAN_REASON_CODES = "ws.cybersource.lan.reason.codes";
public static final String WS_CYBSERSOURCE_TAM_REASON_CODES = "ws.cybersource.tam.reason.codes";

	// Operacion exitosa
public static final int WS_SUCCESSFUL = 0;
// Error no catalogado
public static final int WS_ERROR_NOTDEFINED = -1;
// Error al obtener session Sabre
public static final int WS_ERROR_OPENSESSION = -2;
// Error al cerrar session Sabre
public static final int WS_ERROR_CLOSESESSION = -3;
// No fue definida la propiedad en el HTTP Header para identificar
// a la
// aplicacion
public static final int WS_ERROR_HTTPHEADER = -4;
// Error en los parametros de entrada
public static final int WS_ERROR_PARAMETERS = -5;
// Error en logica del webservice
public static final int WS_ERROR_WEBSERVICE = -6;
// Error al ejecutar operación createData del EJB
public static final int WS_ERROR_EJBCREATEDATA = -7;
// Error al ejecutar operación readData del EJB
public static final int WS_ERROR_EJBREADDATA = -8;
// Error al ejecutar operación updateData del EJB
public static final int WS_ERROR_EJBUPDATEDATA = -9;
// Error al ejecutar operación deleteData del EJB
public static final int WS_ERROR_EJBDELETEDATA = -10;
public static final String ACTION_TAM = "BR";
public static final int INT_ZERO = 0;
public static final String STR_NO = "NO";
public static final String STR_YES = "YES";
public static final String LOGGER_INFO = "INFO ->";
public static final String LOGGER_URL = "INFO URL ->";
public static final String LOGGER_URLWSDL = "INFO URLWSDL ->";
	//G.Carcamo - Valores int para campos MDD. 
public static final int ONE = 1;
public static final int TWO = 2;
public static final int THREE = 3;
public static final int FOUR = 4;
public static final int FIVE = 5;
public static final int SIX = 6;
public static final int SEVEN = 7;
public static final int TEN = 10;
public static final int TWENTY = 20;
public static final int TWENTY_ONE = 21;
public static final int TWENTY_TWO = 22;
public static final int TWENTY_THREE = 23;
public static final int TWENTY_FOUR = 24;
public static final int TWENTY_SEVEN = 27;
public static final int THIRTY_ONE = 31;
public static final int THIRTY_THREE = 33;
public static final int FORTY_TWO = 42;
public static final int FORTY_THREE = 43;
public static final int FORTY_FOUR = 44;
public static final int FORTY_FIVE = 45;
public static final int FORTY_SIX = 46;
public static final int FORTY_SEVEN = 47;
public static final int FORTY_EIGHT = 48;
public static final int FORTY_NINE = 49;
public static final String FLAG_ERROR_LOG = "log.error.flag";
	
	private GlobalConstant() {

	}
}
