package com.latam.pax.cybersrc.impl;

import java.math.BigInteger;
import com.lan.webservice.jaxws.cybersource.validateFraud.AFSService;
import com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory;
import com.lan.webservice.jaxws.cybersource.validateFraud.ReplyMessage;
import com.lan.webservice.jaxws.cybersource.validateFraud.RequestMessage;
import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.client.CyberSourceSSCClient;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.domain.RejectMessage;
import com.latam.pax.cybersrc.domain.schemas.ServiceStatusType;
import com.latam.pax.cybersrc.domain.schemas.ValidationResult;
import com.latam.pax.cybersrc.ssc.utils.ParserRequestSSCUtils;
import com.latam.pax.cybersrc.utils.ParserCommonUtils;
import com.latam.pax.cybersrc.utils.PrintUtils;
import com.latam.pax.cybersrc.utils.XmlTools;

public final class ValidateSSC {
    private ValidateSSC() {

    }

    public static ValidateFraudRS processSSC(ValidateFraudRQ request, AppProperties prop) {
        ValidateFraudRS validateFraudRS = new ValidateFraudRS();
        ValidationResult validationResult = new ValidationResult();
        ServiceStatusType srvStatus = new ServiceStatusType();
        String issueType = request.getSaleInformation().getIsIssue() ? GlobalConstant.EMISSION
                : GlobalConstant.REMISSION;
        String isPayed = request.getSaleInformation().getIsPayed() ? "Y" : "N";
        // Es reemision involutaria?
        String isDisruption = request.getSaleInformation().getIsDisruption() ? "Y" : "N";

        // G.Carcamo - Seccion de llamada a webservice

        // G.Carcamo - Se instancia nuevo rquest message hacia Cybersource nativo.
        ObjectFactory factoryCybersource = new ObjectFactory();
        RequestMessage requestCybersource = factoryCybersource.createRequestMessage();
        AFSService serviceAfs = factoryCybersource.createAFSService();

        // G.Carcamo - Mapeo hacia RQ Nativo.
        ParserRequestSSCUtils.fillPrincipalData(request ,requestCybersource, serviceAfs);

        ParserRequestSSCUtils.fillItems(request, factoryCybersource, requestCybersource);

        ParserRequestSSCUtils.fillMerchantDefinedData(request, issueType, isPayed, isDisruption, factoryCybersource,
                requestCybersource);

        // G.CarcamoCreacion de instancia cliente SSC y asignacion de llamada hacia
        // respuesta
        try {
            PrintUtils.printInfoData(XmlTools.toXml(requestCybersource));
            CyberSourceSSCClient client = new CyberSourceSSCClient(prop);
            ReplyMessage responseCybersource = client.execute(requestCybersource);
            BigInteger reasonCode = responseCybersource.getReasonCode();
            String country = request.getSaleInformation().getSalesOfficeInformation().getCountryISOCode();
            if (ParserCommonUtils.isApproved(reasonCode, prop)) {
                validationResult.setValidationRemark(ParserCommonUtils.getMessage(isDisruption, true, country));
                validationResult.setValid(true);
                validationResult.setRequestId(responseCybersource.getRequestID());
                validateFraudRS.setValidationResult(validationResult);
                srvStatus.setCode(GlobalConstant.WS_SUCCESSFUL);
                srvStatus.setMessage(responseCybersource.getReasonCode() + ":" + responseCybersource.getDecision());
                validateFraudRS.setServiceStatus(srvStatus);
            } else {
                if (null != responseCybersource.getReasonCode()) {
                    RejectMessage rejectMessage = new RejectMessage();
                    rejectMessage.setReasonCode(responseCybersource.getReasonCode().longValue());
                    validationResult.setValidationRemark(ParserCommonUtils.getMessage(isDisruption, false, country));
                    validationResult.setValid(false);
                    validationResult.setRequestId(responseCybersource.getRequestID());
                    validateFraudRS.setValidationResult(validationResult);
                    srvStatus.setCode(GlobalConstant.WS_SUCCESSFUL);
                    srvStatus.setMessage(responseCybersource.getReasonCode() + ":" + responseCybersource.getDecision());
                    validateFraudRS.setServiceStatus(srvStatus);
                }
            }

        } catch (ClientWebServiceException e) {
            PrintUtils.printErrorData("Exception " + e);
            srvStatus.setCode(GlobalConstant.WS_ERROR_WEBSERVICE);
            srvStatus.setMessage("CYBERSOURCE ERROR:" + e);
            validateFraudRS.setServiceStatus(srvStatus);
        }
        return validateFraudRS;
    }
}
