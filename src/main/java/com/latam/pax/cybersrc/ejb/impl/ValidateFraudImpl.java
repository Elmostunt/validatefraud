package com.latam.pax.cybersrc.ejb.impl;

import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.ejb.services.ValidateFraudServices;
import com.latam.pax.cybersrc.impl.ValidateBR;
import com.latam.pax.cybersrc.impl.ValidateSSC;
import com.latam.pax.cybersrc.utils.PrintUtils;

/**
 * @author G.Cárcamo Clase de implementacion para ValidateFraud.Esto para
 *         reemisiones.
 *
 */
public class ValidateFraudImpl implements ValidateFraudServices {

	ValidateBR validateBR;
	ValidateSSC validateSSC;

	@Override
	public ValidateFraudRS execute(ValidateFraudRQ businessData, AppProperties prop) throws ClientWebServiceException {
		if (GlobalConstant.ACTION_TAM
				.equalsIgnoreCase(businessData.getSaleInformation().getSalesOfficeInformation().getCountryISOCode())) {
			PrintUtils.printInfoData(businessData.toString());
			ValidateFraudRQ	request = businessData;			
		 	return ValidateBR.processBR(request,prop);
		} else {
			ValidateFraudRQ	request = businessData;	
			return ValidateSSC.processSSC(request,prop);
		}
	}
}
