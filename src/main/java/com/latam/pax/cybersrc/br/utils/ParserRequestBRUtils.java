package com.latam.pax.cybersrc.br.utils;

import java.math.BigInteger;
import java.util.List;

import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.Itineraries;
import com.latam.pax.cybersrc.domain.schemas.Leg;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.type.DateType;
import com.latam.pax.cybersrc.utils.DateUtils;
import com.latam.pax.cybersrc.utils.ParserCommonUtils;
import com.latam.ws.ics.icstransaction.Item;
import com.latam.ws.ics.icstransaction.MDDField;
import com.latam.ws.ics.icstransaction.MerchantDefinedData;

import com.latam.ws.ics.icstransaction.ObjectFactory;
import com.latam.ws.ics.icstransaction.RequestMessage;

public final class ParserRequestBRUtils extends ParserRequestBRAux {

    public static final String FORMATDATE_Y_M_D_H_M = "yyyy-MM-dd HH:mm";
    public static final String FORMATDATE_Y_M_D = "yyyy-MM-dd";

    private ParserRequestBRUtils() {
        super();
    }

    public static void fillItems(ValidateFraudRQ request, ObjectFactory factoryCybersource,
            RequestMessage requestCybersource) {

        if (requestCybersource.getItem() != null) {
            List<Item> itemsCybersource = requestCybersource.getItem();
            List<com.latam.pax.cybersrc.domain.schemas.Item> itemsBody = request.getSaleInformation()
                    .getShoppingCartInformation().getItems().getItem();
            BigInteger id = BigInteger.ONE;
            for (com.latam.pax.cybersrc.domain.schemas.Item itemBody : itemsBody) {
                Item itemCyberSource = factoryCybersource.createItem();
                itemCyberSource.setId(id);
                itemCyberSource.setPassengerFirstName(itemBody.getPassengerInformation().getFirstName());
                itemCyberSource.setPassengerLastName(itemBody.getPassengerInformation().getLastName());
                itemCyberSource.setPassengerEmail(itemBody.getPassengerInformation().getEmail());
                itemCyberSource.setUnitPrice("0");
                if (!itemBody.getPassengerInformation().getDocuments().getDocument().isEmpty()) {
                    itemCyberSource.setPassengerID(
                            itemBody.getPassengerInformation().getDocuments().getDocument().get(0).getId());
                }
                if (!itemBody.getPassengerInformation().getPhoneNumber().isEmpty()) {
                    itemCyberSource.setPassengerPhone(itemBody.getPassengerInformation().getPhoneNumber());
                }
                itemsCybersource.add(itemCyberSource);
                id = id.add(BigInteger.ONE);
            }

        }
    }

    public static void fillMddFields(ValidateFraudRQ request, String issueType, String isPayed, String isDisruption,
            ObjectFactory factoryCybersource, RequestMessage requestCybersource) {
        MerchantDefinedData merchantDefinedData = factoryCybersource.createMerchantDefinedData();
        List<MDDField> mDDFields = merchantDefinedData.getMddField();
        Itineraries itinerary = getItinerary(request);
        DateUtils dateUtils = new DateUtils();

//        // MDD3
        if (!request.getLegs().getLeg().isEmpty()) {
            ParserCommonUtils.setFieldBR(GlobalConstant.THREE,
                    String.valueOf(dateUtils.getDiference(dateUtils.getDateServer(),
                            dateUtils.transformDateTime(request.getLegs().getLeg().get(GlobalConstant.INT_ZERO)
                                    .getDepartureInformation().getDateTime(), FORMATDATE_Y_M_D_H_M),
                            DateType.HOUR)),
                    factoryCybersource, mDDFields);
        }
        // MDD4
        ParserCommonUtils.setFieldBR(GlobalConstant.FOUR,
                String.valueOf(request.getSaleInformation().getSalesOfficeInformation().getStationNumber()),
                factoryCybersource, mDDFields);
        // MDD10
        if (issueType.equalsIgnoreCase(GlobalConstant.REMISSION)) {
            ParserCommonUtils.setFieldBR(GlobalConstant.TEN,
                    request.getSaleInformation().getIsIssue() ? GlobalConstant.STR_NO : GlobalConstant.STR_YES,
                    factoryCybersource, mDDFields);
        }
        // MDD20
        ParserCommonUtils.setFieldBR(GlobalConstant.TWENTY, itinerary.getItineraryCountry(), factoryCybersource,
                mDDFields);
        // MDD21
        ParserCommonUtils.setFieldBR(GlobalConstant.TWENTY_ONE, itinerary.getCountryInitial(), factoryCybersource,
                mDDFields);
        // MDD22
        ParserCommonUtils.setFieldBR(GlobalConstant.TWENTY_TWO, itinerary.getItineraryAirline(), factoryCybersource,
                mDDFields);
        // MDD23
        ParserCommonUtils.setFieldBR(GlobalConstant.TWENTY_THREE, itinerary.getItineraryClass(), factoryCybersource,
                mDDFields);
        // MDD24
        ParserCommonUtils.setFieldBR(GlobalConstant.TWENTY_FOUR,
                String.valueOf(request.getSaleInformation().getSalesOfficeInformation().getSalesAgentCode()),
                factoryCybersource, mDDFields);
        // G.Carcamo - Nuevos MDD - del 42 al 48
        // MDD42
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_TWO, isPayed, factoryCybersource, mDDFields);
        // MDD43 -- Delta en horas : Fechas de compra (Reemision - Original)
        String originalShopDate = request.getSaleInformation().getExchangeTicketInformation().getIssueDate();
        String actualShopDate = request.getSaleInformation().getExchangeTicketInformation().getIssueDate();
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_THREE,
                String.valueOf(dateUtils.getDiference(dateUtils.transformDateTime(actualShopDate, FORMATDATE_Y_M_D_H_M),
                        dateUtils.transformDateTime(originalShopDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
                factoryCybersource, mDDFields);
        // MDD44 -- Delta en Horas : Fechas de vuelo (Reemision - Original)
        String originalFlightDate = request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()
                .get(0).getDepartureInformation().getDateTime();
        String newFlightDate = request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime();
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_FOUR,
                String.valueOf(dateUtils.getDiference(dateUtils.transformDateTime(newFlightDate, FORMATDATE_Y_M_D_H_M),
                        dateUtils.transformDateTime(originalFlightDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
                factoryCybersource, mDDFields);
        // MDD45 -- Numero del primer cupon que se está cambiando
        int couponNumber = ParserCommonUtils.verifyDiffCoupons(request);
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_FIVE, String.valueOf(couponNumber), factoryCybersource,
                mDDFields);
        // MDD 46 - Numero del boleto original
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_SIX,
                request.getSaleInformation().getExchangeTicketInformation().getTicketNumber(), factoryCybersource,
                mDDFields);
        // MDD 47 - Ruta original - concatenacion de arival - departure de legs
        StringBuilder originalRoute = new StringBuilder();
        for (Leg cursorLeg : request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()) {
            if (!originalRoute.toString().isEmpty()) {
                originalRoute.append(GlobalConstant.STR_TWO_POINT);
            }
            originalRoute.append(cursorLeg.getDepartureInformation().getAirportIATACode() + GlobalConstant.STR_GUION
                    + cursorLeg.getArrivalInformation().getAirportIATACode());
        }
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_SEVEN, originalRoute.toString(), factoryCybersource,
                mDDFields);
        // MDD 48
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_EIGHT,
                String.valueOf(dateUtils.getDiference(dateUtils.getDateServer(),
                        dateUtils.transformDateTime(
                                request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime(),
                                FORMATDATE_Y_M_D_H_M),
                        DateType.HOUR)),
                factoryCybersource, mDDFields);
        // MDD 49
        ParserCommonUtils.setFieldBR(GlobalConstant.FORTY_NINE, isDisruption, factoryCybersource, mDDFields);
        if (mDDFields != null && !mDDFields.isEmpty()) {
            requestCybersource.setMerchantDefinedData(merchantDefinedData);
        }

    }
}
