package com.latam.pax.cybersrc.utils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.latam.pax.cybersrc.domain.type.DateType;

public class DateUtils {

    public static final String DATE_FORMAT_LEGS = "yyyy-MM-dd HH:mm";

    public static String getDateServerString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.now().format(formatter);

    }
    
    public LocalDateTime transformDateTime(String dateTimeString, String format) {
        LocalDateTime localDateTime = LocalDateTime.now();
        if (null != dateTimeString && null != format) {
            localDateTime = LocalDateTime.parse(dateTimeString,
            DateTimeFormatter.ofPattern(format));

        }
        return localDateTime;
    }
    
    public LocalDateTime getDateServer() {
        return LocalDateTime.now();
    }
    
    public long getDiference(LocalDateTime dateInitial, LocalDateTime dateEnd, DateType dateType) {
        PrintUtils.printInfoData("Fecha inicial " + dateInitial + "Fecha final " + dateEnd);
        long dife = Timestamp.valueOf(dateEnd).getTime() - Timestamp.valueOf(dateInitial).getTime();
        return (dife / dateType.getFactor());
    }

}
