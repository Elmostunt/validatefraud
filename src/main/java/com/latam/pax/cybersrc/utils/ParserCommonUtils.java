package com.latam.pax.cybersrc.utils;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import com.latam.arq.commons.properties.AppProperties;
import com.latam.pax.cybersrc.domain.schemas.Leg;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.ws.ics.icstransaction.MDDField;
import com.latam.ws.ics.icstransaction.ObjectFactory;


public final class ParserCommonUtils {

    private ParserCommonUtils() {

    }

    public static String getMessage(String isDisruption, boolean isApproved, String country) {

        String dateTrx = DateUtils.getDateServerString();
        //Mensaje de reemision Involuntaria Parcial
            if("Y".equals(isDisruption)) {
                return involuntaryMessage(isApproved, country,dateTrx);
            }else {
                return voluntaryMessage(isApproved, country,dateTrx);
            }      
    }
    
    private static String voluntaryMessage(boolean isApproved, String country, String dateTrx) {
        if("CL".equals(country)) {
            return (isApproved ? "VMP APROBADA REEMISION COSTO CERO "
                    : "VMP RECHAZADA REEMISION COSTO CERO ") + dateTrx;
        }else if("BR".equals(country)){
            return (isApproved ? "VMP APROVADA REEMISSAO CUSTO ZERO "
                    : "VMP REJEITADA REEMISSÃO CUSTO ZERO") + dateTrx;
        }else {
            return (isApproved ? "VMP APPROVED ZERO COS EXCHANGE "
                    : "VMP REJECTED ZERO COST EXCHANGE ") + dateTrx;
        }
    }

    public static String involuntaryMessage(boolean isApproved, String country, String dateTrx) {
   
        if("CL".equals(country)) {
            return (isApproved ? "VMP APROBADA REEMISION INVOLUNTARIA "
                    : "VMP RECHAZADA REEMISIÓN INVOLUNTARIA ") + dateTrx;
        }else if("BR".equals(country)){
            return (isApproved ? "VMP APROVADA REEMISSAO INVOLUNTARIA "
                    : "VMP REJEITADA REEMISSÃO INVOLUNTÁRIA ") + dateTrx;
        }else {
            return (isApproved ? "VMP APROVADA REEMISSAO INVOLUNTARIA "
                    : "VMP REJEITADA REEMISSÃO INVOLUNTÁRIA ") + dateTrx;
        }
    }
    
    public static boolean isApproved(BigInteger reasonCodeResult, AppProperties prop) {
        PrintUtils.printInfoData(reasonCodeResult.toString());
        if (prop.getTamReasonCode() == null) {
            PrintUtils.printErrorData(prop.getTamReasonCode() + " property not found in properties of application");
            return false;
        }
        String[] reasonCodesArray = prop.getTamReasonCode().split(",");
        List<String> reasonCodesList = Arrays.asList(reasonCodesArray);
        for (String reasonCode : reasonCodesList) {
            if (new BigInteger(reasonCode).longValue() == reasonCodeResult.longValue()) {
                return true;
            }
        }
        return false;
    }
    

    public static int verifyDiffCoupons(ValidateFraudRQ request) {
        int couponNumber = 0;
        int lenghtLegs = request.getLegs().getLeg().size();
        int factor = 0;
        Boolean foundDifferent = Boolean.FALSE;
        List<Leg> legOriginal = request.getLegs().getLeg();
        List<Leg> legReemision = request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg();
        while (foundDifferent.equals(Boolean.FALSE) && factor < lenghtLegs) {
            if (!legOriginal.get(factor).equals(legReemision.get(factor))) {
                foundDifferent = Boolean.TRUE;
                couponNumber = factor;
            } else {
                factor++;
            }

        }

        return couponNumber;
    }
    
    public static void setFieldSSC(int id, String value, com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory factoryCybersource, List<com.lan.webservice.jaxws.cybersource.validateFraud.MDDField> mDDFields) {
        if (value != null) {
            com.lan.webservice.jaxws.cybersource.validateFraud.MDDField mddField = factoryCybersource.createMDDField();
            mddField.setId(BigInteger.valueOf(id));
            mddField.setValue(value);
            mDDFields.add(mddField);
        }
    }
    
    public static void setFieldBR(int id, String value, ObjectFactory factoryCybersource, List<MDDField> mDDFields) {
        if (value != null) {
            MDDField mddField = factoryCybersource.createMDDField();
            mddField.setId(BigInteger.valueOf(id));
            mddField.setValue(value);
            mDDFields.add(mddField);
        }
    }
}
