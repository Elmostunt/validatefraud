package com.latam.arq.commons.ws.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import com.lan.webservice.jaxws.cybersource.validateFraud.ITransactionProcessor;
import com.lan.webservice.jaxws.cybersource.validateFraud.ReplyMessage;
import com.lan.webservice.jaxws.cybersource.validateFraud.RequestMessage;
import com.lan.webservice.jaxws.cybersource.validateFraud.TransactionProcessor;
import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.client.handler.WSSUsernameTokenSecurityHandler;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.utils.PrintUtils;
import com.latam.pax.cybersrc.utils.XmlTools;

import lombok.Getter;
import lombok.Setter;

public class CyberSourceSSCClient {

    ITransactionProcessor port;
    @Getter
    @Setter
    private String userService;
    @Getter
    @Setter
    private String passService;

    public CyberSourceSSCClient(AppProperties prop) throws ClientWebServiceException {
        try {
            URL urlWsdl = new URL(prop.getUrlLanCybersource());
            PrintUtils.printInfoData(urlWsdl.toString());

            setUserService(prop.getSecLanSSCUsername());
            setPassService(prop.getLanSSCCodeB64());

            HandlerResolver handlerResolver = val -> {
                List<Handler> handlerListBr = new ArrayList<>();
                handlerListBr.add(new WSSUsernameTokenSecurityHandler(userService, passService));
                return handlerListBr;
            };

            QName serviceNameSsc = new QName(GlobalConstant.WS_QNAME_ATTRIBUTE_ONE,
                    GlobalConstant.WS_QNAME_ATTRIBUTE_TWO);

            TransactionProcessor transactionProcessorSsc = new TransactionProcessor(urlWsdl, serviceNameSsc);
            transactionProcessorSsc.setHandlerResolver(handlerResolver);
            port = transactionProcessorSsc.getPortXML();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                    prop.getEndpointLanCybersource());

        } catch (MalformedURLException e) {
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ClientWebServiceException(e.getMessage(), null);
        }
    }

    public ReplyMessage execute(RequestMessage requestMessage) throws ClientWebServiceException {
        try {
            PrintUtils.printInfoData("INFO : request => " + XmlTools.toXml(requestMessage));
            return port.runTransaction(requestMessage);
        } catch (WebServiceException e) {
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ClientWebServiceException(e.getMessage(), null);
        }

    }
}
