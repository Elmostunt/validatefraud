package com.latam.arq.commons.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.latam.arq.commons.properties.AppProperties;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.ejb.impl.ValidateFraudImpl;
import com.latam.pax.cybersrc.utils.PrintUtils;
import com.latam.pax.cybersrc.utils.XmlTools;

/**
 * @author G.Carcamo Clase de controladores REST. El primero gestiona las
 *         peticiones JSON, mapeando a objetos RQ y RS del servicio. Se apunta a
 *         localhost:8081/controller.
 */
@ComponentScan("com.latam")
@RestController
public class ApiController {

	PrintUtils print;
	@Autowired
	AppProperties prop;

	ValidateFraudImpl validate = new ValidateFraudImpl();

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/controller")
	public ResponseEntity<ValidateFraudRS> validate(@RequestBody ValidateFraudRQ validateFormOfPaymentRQ)
			throws ClientWebServiceException {

		PrintUtils.printInfoData("RQ INPUT ->" + XmlTools.toXml(validateFormOfPaymentRQ));
		ValidateFraudRS response = validate.execute(validateFormOfPaymentRQ, prop);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
