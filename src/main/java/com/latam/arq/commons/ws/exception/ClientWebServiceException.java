package com.latam.arq.commons.ws.exception;

public class ClientWebServiceException extends Exception {

	private static final long serialVersionUID = -6446379435910847470L;

	public ClientWebServiceException() {
		super();
	}

	public ClientWebServiceException(String message) {
		super(message);
	}

	public ClientWebServiceException(Throwable cause) {
		super(cause);
	}

	public ClientWebServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
