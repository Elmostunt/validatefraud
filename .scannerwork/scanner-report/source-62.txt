package com.latam.pax.cybersrc.ssc.utils;

import java.math.BigInteger;
import java.util.List;

import com.lan.webservice.jaxws.cybersource.validateFraud.MDDField;
import com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory;
import com.lan.webservice.jaxws.cybersource.validateFraud.RequestMessage;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.Leg;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.type.DateType;
import com.latam.pax.cybersrc.utils.DateUtils;

public class ParserRequestSSCUtilsAux {
	
	public static final String FORMATDATE_Y_M_D_H_M = "yyyy-MM-dd HH:mm";
	public static final String FORMATDATE_Y_M_D = "yyyy-MM-dd";


	public static void fillMerchantDefinedData(ValidateFraudRQ request, String issueType, String isPayed,
			String isDisruption, ObjectFactory factoryCybersource,
			RequestMessage requestCybersource) {
		DateUtils dateUtils = new DateUtils();
		com.lan.webservice.jaxws.cybersource.validateFraud.MerchantDefinedData merchantDefinedData = factoryCybersource
				.createMerchantDefinedData();
		List<com.lan.webservice.jaxws.cybersource.validateFraud.MDDField> mDDFields = merchantDefinedData.getMddField();
		setField(GlobalConstant.ONE,
				request.getSaleInformation().getShoppingCartInformation().getItems().getItem().get(0).getPnrCode(),
				factoryCybersource, mDDFields);
		setField(GlobalConstant.TWO,
				request.getSaleInformation().getSalesOfficeInformation().getCountryISOCode(), factoryCybersource,
				mDDFields);
		setField(GlobalConstant.FIVE,
				request.getSaleInformation().getSalesOfficeInformation().getSalesAgentCode(), factoryCybersource,
				mDDFields);
		setField(GlobalConstant.SIX,
				request.getSaleInformation().getSalesOfficeInformation().getStationNumber(), factoryCybersource,
				mDDFields);
		setField(GlobalConstant.SEVEN, request.getLegs().getLeg().get(0).getFareClass(),
				factoryCybersource, mDDFields);
		setField(GlobalConstant.TWENTY_SEVEN, issueType, factoryCybersource, mDDFields);
		if (!request.getLegs().getLeg().isEmpty()) {
			setField(GlobalConstant.THIRTY_ONE,
					request.getLegs().getLeg().get(0).getAirlineIATACode(), factoryCybersource, mDDFields);
			setField(GlobalConstant.THIRTY_THREE,
					request.getLegs().getLeg().get(0).getFlightNumber(), factoryCybersource, mDDFields);
		}
		// G.Carcamo - Nuevos MDD - del 42 al 48
		setField(GlobalConstant.FORTY_TWO, isPayed, factoryCybersource, mDDFields);
		// MDD 43 - Delta en horas de compra. (Reemision-Original)
		String reemisionShopDate = request.getSaleInformation().getCurrentDateTime();
		String originalShopDate = request.getSaleInformation().getExchangeTicketInformation().getIssueDate();
		setField(GlobalConstant.FORTY_THREE,
				String.valueOf(
						dateUtils.getDiference(dateUtils.transformDateTime(originalShopDate, FORMATDATE_Y_M_D_H_M),
								dateUtils.transformDateTime(reemisionShopDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
				factoryCybersource, mDDFields);
		// MDD 44 - Delta en horas de fecha de vuelo (Reemison-Original)
		String originalFlightDate = request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()
				.get(0).getDepartureInformation().getDateTime();
		String reemisionFlightDate = request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime();
		setField(GlobalConstant.FORTY_FOUR,
				String.valueOf(
						dateUtils.getDiference(dateUtils.transformDateTime(reemisionFlightDate, FORMATDATE_Y_M_D_H_M),
								dateUtils.transformDateTime(originalFlightDate, FORMATDATE_Y_M_D_H_M), DateType.HOUR)),
				factoryCybersource, mDDFields);
		// MDD 45 - Numero del primer cupon que se esta cambiando
		int couponNumber = verifyDiffCoupons(request);
		setField(GlobalConstant.FORTY_FIVE, String.valueOf(couponNumber), factoryCybersource,
				mDDFields);
		// MDD 46 - Numero del boleto original
		setField(GlobalConstant.FORTY_SIX,
				request.getSaleInformation().getExchangeTicketInformation().getTicketNumber(), factoryCybersource,
				mDDFields);
		// MDD 47 - Ruta original - concatenacion de arival - departure de legs
		StringBuilder originalRoute = new StringBuilder();
		for (Leg cursorLeg : request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg()) {
			if (!originalRoute.toString().isEmpty()) {
				originalRoute.append(GlobalConstant.STR_TWO_POINT);
			}
			originalRoute.append(cursorLeg.getDepartureInformation().getAirportIATACode() + GlobalConstant.STR_GUION
					+ cursorLeg.getArrivalInformation().getAirportIATACode());
		}
		setField(GlobalConstant.FORTY_SEVEN, originalRoute.toString(), factoryCybersource,
				mDDFields);
		// MDD 48
		setField(GlobalConstant.FORTY_EIGHT,
						String.valueOf(
								dateUtils.getDiference(
										dateUtils.transformDateTime(request.getLegs().getLeg().get(0)
												.getDepartureInformation().getDateTime(), FORMATDATE_Y_M_D_H_M),
										dateUtils.getDateServer(), DateType.HOUR)),
						factoryCybersource, mDDFields);
		// MDD 49
		setField(GlobalConstant.FORTY_NINE, isDisruption, factoryCybersource, mDDFields);
		if (mDDFields != null && !mDDFields.isEmpty()) {
			requestCybersource.setMerchantDefinedData(merchantDefinedData);
		}

	}
	
	private static int verifyDiffCoupons(ValidateFraudRQ request) {
		int couponNumber = 0;
		int lenghtLegs = request.getLegs().getLeg().size();
		Boolean foundDifferent = Boolean.FALSE;
		List<Leg> legOriginal = request.getLegs().getLeg();
		List<Leg> legReemision = request.getSaleInformation().getExchangeTicketInformation().getLegs().getLeg();
		for (int factor = 0; factor <= lenghtLegs; factor++) {
			while (foundDifferent.equals(Boolean.FALSE)) {
				if (!legOriginal.get(factor).equals(legReemision.get(factor))) {
					foundDifferent = Boolean.TRUE;
					couponNumber = factor;
				}
			}
		}
		return couponNumber;
	}
	public static void setField(int id, String value,
			com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory factoryCybersource,
			List<MDDField> mDDFields) {
		if (value != null) {
			MDDField mddField = factoryCybersource.createMDDField();
			mddField.setId(BigInteger.valueOf(id));// SI
			mddField.setValue(value);
			mDDFields.add(mddField);
		}
	}
}
