package com.latam.pax.cybersrc.impl;

import org.springframework.core.env.Environment;

import com.lan.webservice.jaxws.cybersource.validateFraud.AFSService;
import com.lan.webservice.jaxws.cybersource.validateFraud.ObjectFactory;
import com.lan.webservice.jaxws.cybersource.validateFraud.ReplyMessage;
import com.lan.webservice.jaxws.cybersource.validateFraud.RequestMessage;
import com.latam.arq.commons.ws.client.CyberSourceSSCClient;
import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.domain.schemas.ServiceStatusType;
import com.latam.pax.cybersrc.domain.schemas.ValidationResult;
import com.latam.pax.cybersrc.ssc.ejb.services.ValidateSSCServices;
import com.latam.pax.cybersrc.ssc.utils.ParserRequestSSCUtils;
import com.latam.pax.cybersrc.utils.PrintUtils;

public class ValidateSSC implements ValidateSSCServices {
	public ValidateFraudRS processSSC(ValidateFraudRQ request, Environment env) {
		ValidateFraudRS validateFraudRS = new ValidateFraudRS();
		ValidationResult validationResult = new ValidationResult();
		ServiceStatusType srvStatus = new ServiceStatusType();
		String issueType = request.getSaleInformation().getIsIssue() ? GlobalConstant.EMISSION : GlobalConstant.REMISSION;
		String isPayed = request.getSaleInformation().getIsPayed() ? "Y" : "N";
		//Es reemision involutaria?
		String isDisruption = request.getSaleInformation().getIsDisruption() ?"Y":"N";

		try {
			// G.Carcamo - Seccion de llamada a webservice

			// G.Carcamo - Se instancia nuevo rquest message hacia Cybersource nativo.
			ObjectFactory factoryCybersource = new ObjectFactory();
			RequestMessage requestCybersource = factoryCybersource.createRequestMessage();
			AFSService serviceAfs = factoryCybersource.createAFSService();

			// G.Carcamo - Mapeo hacia RQ Nativo.
			ParserRequestSSCUtils.fillPrincipalData(requestCybersource, serviceAfs, env);
//------------------------Cybersourrequest filled from abu
			ParserRequestSSCUtils.fillItems(request, factoryCybersource, requestCybersource);
			ParserRequestSSCUtils.fillMerchantDefinedData(request, issueType, isPayed, isDisruption,
					factoryCybersource, requestCybersource);
			
			// G.CarcamoCreacion de instancia cliente SSC y asignacion de llamada hacia respuesta
			CyberSourceSSCClient client = new CyberSourceSSCClient(env);
			ReplyMessage responseCybersource = client.execute(requestCybersource);
			
			srvStatus.setCode(responseCybersource.getReasonCode().intValue());
			srvStatus.setMessage("OK");
			srvStatus.setNativeMessage("OK");
			validationResult.setValid(true);
			validateFraudRS.setServiceStatus(srvStatus);
			validateFraudRS.setValidationResult(validationResult);
			
		} catch (ClientWebServiceException e) {
			PrintUtils.printErrorData("Exception "+e);			
			srvStatus.setCode(GlobalConstant.WS_ERROR_WEBSERVICE);
			srvStatus.setMessage("CYBERSOURCE ERROR:" + e);
			validateFraudRS.setServiceStatus(srvStatus);
		}
		return validateFraudRS;
	}

}
