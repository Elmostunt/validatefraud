package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class ArrivalInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5312206578660305070L;
	@Getter
	@Setter
	String dateTime;
	@Getter
	@Setter
	String airportIATACode;
	@Getter
	@Setter
	String countryISOCode;
}
