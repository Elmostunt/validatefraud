package com.latam.ejemplo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import com.latam.pax.cybersrc.domain.ExampleObject;

/**
 * @author G.Carcamo Clase test para conexión REST. Basta con cambiar formato de
 *         entrada y hacer calzar con su implementacion
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiControllerTest {

	// G.Carcamo - @InyectMocks indica que Rest template apuntara a mockUp Local.
	@InjectMocks
	RestTemplate restTemplate = new RestTemplate();

	@Test
	public void testRest() {
		// G.Carcamo - Se definen header en caso de requerirlos
		// HttpHeaders headers = new HttpHeaders();
		// headers.setContentType(MediaType.APPLICATION_JSON);

		// G.Carcamo - Se apunta envio de objeto test a endopoint local validando
		// controlador.
		String url = "http://localhost:8081/string";
		System.out.println("Log Ejemplo");
		


		// G.Carcamo - Se genera objeto de ejemplo, que contiene string. Dado envio de
		// este objeto se valida la comunicacion
		ExampleObject example = new ExampleObject();
		example.setMessage("");

		// G.Carcamo - Se realiza llamada a servicio Rest dada Url, request y tipo de
		// objeto de salida
		String responseEntity = restTemplate.postForObject(url, example, String.class);
		System.out.println("RS:" + responseEntity);
	}
}