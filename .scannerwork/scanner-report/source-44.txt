package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ValidateFraudRS implements Serializable {
    
    private static final long serialVersionUID = -7654429842676328314L;
    
    @NotNull( message = "{domain.not.null}" )
    @Getter@Setter
    private ServiceStatusType serviceStatus;
    @Getter@Setter
    private ValidationResult validationResult;
}