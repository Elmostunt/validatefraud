package com.latam.pax.cybersrc.ejb.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.latam.arq.commons.ws.exception.ClientWebServiceException;
import com.latam.pax.cybersrc.br.ejb.services.ValidateBRServices;
import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRS;
import com.latam.pax.cybersrc.ejb.services.ValidateFraudServices;
import com.latam.pax.cybersrc.ssc.ejb.services.ValidateSSCServices;

/**
 * @author G.Cárcamo Clase de implementacion para ValidateFraud.Esto para
 *         reemisiones.
 *
 */
public class ValidateFraudImpl implements ValidateFraudServices {
	@Autowired
	Environment env;

	ValidateBRServices validateBRServices;
	ValidateSSCServices validateSSCServices;

	@Override
	public ValidateFraudRS execute(ValidateFraudRQ businessData) throws ClientWebServiceException {
		if (GlobalConstant.ACTION_TAM
				.equalsIgnoreCase(businessData.getSaleInformation().getSalesOfficeInformation().getCountryISOCode())) {
			return validateBRServices.processBR(businessData, env);
		} else {
			return validateSSCServices.processSSC(businessData, env);
		}
	}

}
